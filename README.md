# Drone Postgres Example

This example app shows how to attach to a Postgres service instance in
[Drone](https://github.com/drone/drone).

## About This Example

The example uses [goose](http://bitbucket.org/liamstask/goose) to load
all of the SQL in `db/migrations` into the database.

Take a look ad `db/dbconf.yml` to see how we connect to the database.

Blah
